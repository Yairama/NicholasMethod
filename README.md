# Application of the nicholas algorithm for the selection of the ideal mining method.

This code is a small program created by @Yairama that uses
the nicolás method to determine the type of exploitation method to be used in a mine.
The entire graphical interface is programmed in java swing and no additional libraries are used.

## How to use

-In order to use the program it is necessary to have [Java](https://www.java.com/es/download/) installed.

-You can download the already compiled program from the following [link](https://drive.google.com/drive/folders/1bJaWH1tzEO5ud8vJ3awrHlv82mYJN-uH?usp=sharing)

-If you want the code it will be found inside _Metodo de nicholás Folder_

## About

Autor: [Malmco Yair Camborda Morocho](https://www.linkedin.com/in/yairama/) - Mining engineering student


### License
[MIT](https://choosealicense.com/licenses/mit/)
